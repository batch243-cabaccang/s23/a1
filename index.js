function Trainer(name, age, pokemon1, pokemon2, pokemon3, friend1) {
    this.name = name;
    this.age = age;
    this.pokemon = [pokemon1, pokemon2, pokemon3];
    this.friends = [{ friend1 }];
    this.talk = function (pokemon) {
        console.log(`I choose you, ${pokemon.name}!`);
      };
  }
  
  function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = Math.floor(0.3 * level);
    this.tackle = function (target) {
      console.log(`${this.name} tackled ${target.name}`);
      console.log(
        `${target.name}'s health is now reduced to ${
          target.health - this.attack
        } `
      );
      target.health -= this.attack;
      if (target.health <= 0) {
        console.log(`${target.name} Fainted! Oh, no! Haha`);
      }
    };
  }
  const bulbasur = new Pokemon("Bulbasur", 90);
  const squirtel = new Pokemon("Squirtel", 90);
  const charmander = new Pokemon("Charmander", 90);
  
  const jane = new Trainer("Jane", "18", bulbasur, squirtel, charmander); // No friends
  const john = new Trainer("John", "20", squirtel, charmander, bulbasur, jane);
  console.log(john);
  